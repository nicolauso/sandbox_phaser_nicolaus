import { GameScene } from "./scenes/game"

export type Config = {
    scene: GameScene,
    x: number,
    y: number,
    texture: string
}