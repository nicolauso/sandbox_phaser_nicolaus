

export class GameScene extends Phaser.Scene
{
    test: Phaser.GameObjects.Image

    preload()
    {
        // load assets here: ex:
        this.load.image('character', 'assets/images/sprites/character/walk/1-character.png')
        // this.load.image('test', 'assets/test.png');
    }

    create()
    {
        // create things here
        this.test = this.add.image(400, 300, 'character')
        // this.test  = this.add.image(400, 300, 'test');
    }

    update(time: number, delta: number): void {
        // this.test.rotation += 0.01;
    }
}